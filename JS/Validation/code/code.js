const root = document.getElementById("root")
window.addEventListener("DOMContentLoaded", () => {

    const inputs = [
        elements("text", "Введите Ваше имя", "", /^[А-яіїґє-]+$/),
        elements("tel", "Введите номер телефона", "", /^\+38\d{3}-\d{3}-\d{2}-\d{2}$/),
        elements("email", "Введите Ваш емейл", "", /\b[a-z0-9._]+@[a-z0-9.-]+\.[a-z]{2,4}\b/),
        elements("password", "Введите Ваш пароль", "", /(?=^.{6,}$)(?=.*[0-9])(?=.*[A-Z])(?=.*[a-z])(?=.*[^A-Za-z0-9])/g, "password"),
        elements("password", "Подтвердите Ваш пароль", "", /(?=^.{6,}$)(?=.*[0-9])(?=.*[A-Z])(?=.*[a-z])(?=.*[^A-Za-z0-9])/g, "confirm_password"),
        elements("submit")
    ]

    root.append(...inputs)

})

const elements = function (type, placeholder = "", className = "", pattern, name = "") {
    const input = document.createElement("input");
    const label = document.createElement("label");
    const div = document.createElement("div")


    input.addEventListener("change", () => {
        validateInput(input, pattern)
    })

    input.setAttribute("type", type);
    input.setAttribute("placeholder", placeholder);
    input.setAttribute("name", name)
    input.className = className;
    label.innerText = placeholder;
    label.append(input);
    div.append(label);
    return div;
}

root.addEventListener("submit", checkSubmit);

function checkSubmit(e) {
    let invalid = false;
    for (let i = 0; i < root.elements.length; i++) {
        if (root.elements[i].type !== "submit") {
            if (root.elements[i].className === "error" || root.elements[i].className === "") {
                invalid = true;
            }
        }
    }
    if (invalid) {
        alert("Допущены ошибки при заполнении формы.");
        e.preventDefault()
    }else if (!passwordValid()) {
        alert("Пароли не соответсвуют.");
        e.preventDefault()
    }
    else {
        localStorageInput()
    }
}


function validateInput(input, pattern) {
    let res = pattern.test(input.value);
    if (res === true) {
        input.className = "valid";
    }
    else {
        input.className = "error";
    }

}
function passwordValid() {
    let x = document.getElementsByName('password')[0].value,
        y = document.getElementsByName('confirm_password')[0].value;

    if (x === y) {
        return true;
    }
    else {
        return false;
    }
}
function localStorageInput() {
    const name = document.querySelector('[type=text]').value;
    const tel = document.querySelector('[type=tel]').value;
    const email = document.querySelector('[type=email]').value;
    const passwor = document.querySelector('[type=password]').value;
    localStorage.name = name;
    localStorage.tel = tel;
    localStorage.email = email;
    localStorage.passwor = passwor;
    localStorage.time = new Date();

}

// Проверка данных. Используя JS cоздайте 5 полей для воода данных и остаилизуйте их 
// Добавьте стили на ошыбку и стиль на верный ввод. Поля: Имя (Укр. Буквы), 
// Номер телефона в формате +38ХХХ-ХХХ-ХХ-ХХ, електронная почта, пароль и 
// подтверждение пароля (пароли должны совпадать ). Реазизуй проверку данных. 
// При вводе данных сразу проверять на правильность и выводить ошибку если такая необходима. 
// добавь кнопку регистрация, при нажатии кнопки проверить все поля и вывести ошибку если
//  такая будет. Сохраниение. Если пользователь все записал верно, то сохраните данные на 
//  клиенте с указанием даты и времени сохранения. Для стилизации используй CSS классы.
//   Для создания элементов используй JS.