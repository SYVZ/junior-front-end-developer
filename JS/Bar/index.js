let listMoney = [{ name: "UAH", sum: 100 }, { name: "USD", sum: 100 }, { name: "CAD", sum: 200 }];
let newListMoney;


function percentages(list) {
    let total = 0;
    for (let i = 0; i < list.length; i++) {
        total += list[i].sum;
    }
    newListMoney = list.map(function (x) {
        return { percent: parseFloat((x.sum * 100 / total).toFixed(2)), name: x.name, sum: x.sum };
    });
};

listMoney.push({ name: "BIF", sum: 300 })

percentages(listMoney);

const color = () => {
    let r = Math.floor(Math.random() * (256)),
        g = Math.floor(Math.random() * (256)),
        b = Math.floor(Math.random() * (256));
    return '#' + r.toString(16) + g.toString(16) + b.toString(16);
}


function outPutMoneyBar(list) {
    for (let i = 0; i < list.length; i++) {
        let moneyBar = document.querySelector(".money");
        let span = document.createElement("span");
        span.setAttribute("title", `${list[i].name} ${list[i].sum}`);
        span.style.backgroundColor = `${color()}`;
        span.style.width = `${list[i].percent}%`;
        span.classList.add("moneylist")
        moneyBar.append(span);

    };
};

outPutMoneyBar(newListMoney)