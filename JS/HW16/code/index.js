function Human(age, name) {
    this.age = age;
    this.name = name;
};

let g = [new Human(12, "Андрей"), new Human(8, "Катя"), new Human(18, "Вася"), new Human(6, "Таня")];

function bubbleSort(arr) {
    for (let j = arr.length - 1; j > 0; j--) {
        for (let i = 0; i < j; i++) {
            if (arr[i].age > arr[i + 1].age) {
                let temp = arr[i];
                arr[i] = arr[i + 1];
                arr[i + 1] = temp;
            }
        }
    }
    return arr;
};
bubbleSort(g);
console.log(g);

function Worker(name, yearofbirth, occupation) {
    // свойства экземпляра
    this.name = name;
    this.yearofBirth = yearofbirth;
    this.occupation = occupation;
     // метод экземпляра
    this.age = function () {
        document.write(this.name + ": возраст " + (new Date().getFullYear() - this.yearofBirth) + "<br>");
    }

};
// Worker.prototype.age = function () {
//     document.write(this.name + ": возраст " + (new Date().getFullYear() - this.yearofBirth) + "<br>");
// }


// создание экземпляров и работа с свойствами и методами экземпляров.
let Andrey = new Worker("Андрей", 1995, "manager");
let Kate = new Worker("Катя", 1989, "reseptionist");
let Vasya = new Worker("Вася", 1996, "clerk");
let Tanya = new Worker("Таня", 1977, "secretary");

Vasya.occupation = "manager";
Vasya.age();
Tanya.age();

// свойство функции конструктора 
Worker.vacation = 31;

// Метод функции-конструктора
Worker.newPeople = function () {
    return new Worker("Сергей", 1987, "engineer")
};
// работа с свойством и методом функции-конструктора.
Worker.vacation = 28;
Worker.newPeople().age();




