// Создайте приложение секундомер.

//* Секундомер будет иметь 3 кнопки "Старт, Стоп, Сброс"
//* При нажатии на кнопку стоп фон секундомера должен быть красным, старт - зеленый, сброс - серый
//* Вывод счётчиков в формате ЧЧ:ММ:СС
//* Реализуйте Задание используя синтаксис ES6 и стрелочные функции
// let timerInput = document.getElementById("time"); // Берём строку
// let buttonRun = document.getElementById("button");// Берём кнопку запуска
// let timerShow = document.getElementById("timer"); // Берём блок для показа времени

// buttonRun.addEventListener('click', function() {
//     timeMinut = parseInt(timerInput.value) * 60
// })

// timer = setInterval(function () {
//     let seconds = timeMinut%60 // Получаем секунды
//     let minuts = timeMinut/60%60 // Получаем минуты
//     let hour = timeMinut/60/60%60 // Получаем часы
//     // Условие если время закончилось то...
//     if (timeMinut <= 0) {
//         // Таймер удаляется
//         clearInterval(timer);
//         // Выводит сообщение что время закончилось
//         alert("Время закончилось");
//     } else { // Иначе
//         // Создаём строку с выводом времени
//         let strTimer = `${Math.trunc(hour)}:${Math.trunc(minuts)}:${seconds}`;
//         // Выводим строку в блок для показа таймера
//         timerShow.innerHTML = strTimer;
//     }
//     --timeMinut; // Уменьшаем таймер
// }, 1000)

const hourElement = document.querySelector('.hour')
const minuteElement = document.querySelector('.minute')
const secondElement = document.querySelector('.second')
const milisecondElement = document.querySelector('.milisecond')

const startButton = document.querySelector('.start')
const stopButton = document.querySelector('.stop')
const pauseButton = document.querySelector('.pause')
const newButton = document.querySelector('.new')

startButton.addEventListener('click', () => {
    clearInterval(interval)
    interval = setInterval(startTimer, 10)
})

pauseButton.addEventListener('click', () => {
    clearInterval(interval)
})

stopButton.addEventListener('click', () => {
    clearInterval(interval)
    clearFields()

})
newButton.addEventListener('click', () => {
    clearInterval(interval)
    newResult()
    clearFields()
    clearInterval(interval)
    interval = setInterval(startTimer, 10)
})

let hour = 00,
    minute = 00,
    second = 00,
    milisecond = 00,
    interval,
    counter = 0,
    resultHour = 00,
    resultMinute = 00,
    resultSecond = 00,
    resultMilisecond = 00

function startTimer() {
    milisecond++
    if (milisecond < 9) {
        resultMilisecond = '0' + milisecond
        milisecondElement.innerHTML = '0' + milisecond
    }
    if (milisecond > 9) {
        resultMilisecond = milisecond
        milisecondElement.innerHTML = milisecond
    }
    if (milisecond > 99) {
        second++
        secondElement.innerHTML = '0' + second
        milisecond = 0
        milisecondElement.innerHTML = '0' + milisecond

    }

    if (second < 9) {
        resultSecond = '0' + second
        secondElement.innerHTML = '0' + second
    }
    if (second > 9) {
        resultSecond = second
        secondElement.innerHTML = second
    }
    if (second > 59) {
        resultSecond = '0' + second
        minute++
        minuteElement.innerHTML = '0' + minute
        second = 0
        secondElement.innerHTML = '0' + second
    }

    if (minute < 9) {
        resultMinute = '0' + minute
        minuteElement.innerHTML = '0' + minute
    }
    if (minute > 9) {
        resultMinute = minute
        minuteElement.innerHTML = minute
    }
    if (minute > 59) {
        hour++
        hourElement.innerHTML = '0' + hour
        minute = 0
        minuteElement.innerHTML = '0' + minute
    }

    if (hour < 9) {
        resultHour = '0' + hour
        hourElement.innerHTML = '0' + hour
    }
    if (hour > 9) {
        resultHour =  hour
        hourElement.innerHTML = hour
    }

}

function newResult() {
    counter++

    const results = document.querySelector('.results')
    const block = document.createElement('div')
    block.innerHTML = `Results ${counter}: ${resultHour}:${resultMinute}:${resultSecond}:${resultMilisecond}`
    results.append(block)
    block.classList.add("results__info")
}

function clearFields() {
    hour = 00
    minute = 00
    second = 00
    milisecond = 00
    hourElement.textContent = '00'
    minuteElement.textContent = '00'
    secondElement.textContent = '00'
    milisecondElement.textContent = '00'
}