const buttonPaint = document.querySelector(".buttonPaint")
buttonPaint.onclick = () => {
    let diametr = parseInt(prompt("Укажите диаметр круга от 10 и больше:", "10"));
    if (diametr === Number || diametr > 9) {
        for (let i = 0; i < 100; i++) {
            let around = document.getElementById("around");
            let a = document.createElement("input");
            const color = ["red", "orange", "yellow", "green", "blue", "pink"];
            let randomColor = color[Math.floor(Math.random() * 6)];
            a.setAttribute("type", "button");
            a.setAttribute("value", `    `);
            a.setAttribute("onclick", "deleteSelf(this)");
            a.style.backgroundColor = `${randomColor}`;
            a.style.padding = `${diametr}px`;
            a.classList.add("round")
            around.append(a);
        };
    }
};
const deleteSelf = (button) => {
    button.remove();

};