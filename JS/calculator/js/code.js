window.addEventListener("DOMContentLoaded", function () {
    const keys = document.querySelector('.keys');


    keys.addEventListener("click", function (e) {
        validate(e.target.value);
        show();
    })

})
let calculate = {
    operand1: '',
    operand2: '',
    sign: '',
    result: '',
    rememberNumber: '0',
    mrc: ''
};

function validate(value) {
    if ((/\d/.test(value) || (/[.]/.test(value) && !calculate.operand1.includes('.'))) && calculate.sign === '') {
        if (calculate.operand1 === '' && /[.]/.test(value)) {
            calculate.operand1 = '0' + value;
        } else {
            calculate.operand1 += value;
        }
    } else if (/[+-/*]/.test(value) && calculate.operand1 !== '' && /[^.]/.test(value) && (value !== 'm+' && value !== 'm-')) {
        if (calculate.operand2 !== '') {
            calculate.result = eval(`${calculate.operand1} ${calculate.sign} ${calculate.operand2}`);
            calculate.operand1 = calculate.result;
            calculate.sign = value;
            calculate.result = '';
            calculate.operand2 = '';
        } else calculate.sign = value;
    } else if ((/\d/.test(value) || (/[.]/.test(value) && !calculate.operand2.includes('.'))) && calculate.operand1 !== '' && calculate.sign !== '') {
        if (calculate.operand2 === '' && /[.]/.test(value)) {
            calculate.operand2 = '0' + value;
        } else {
            calculate.operand2 += value;
        }
    } else if (/[=]/.test(value) && calculate.operand1 !== '' && calculate.sign !== '' && calculate.operand2 !== '') {
        calculate.result = eval(`${calculate.operand1} ${calculate.sign} ${calculate.operand2}`)

    } else if (/[C]/.test(value)) {
        reset()
    } else if (value === 'm+') {
        if (calculate.operand1 !== '' && calculate.operand2 == '') {
            calculate.rememberNumber = eval(`${calculate.rememberNumber}+${calculate.operand1}`)
            console.log('m+/operand1' + calculate.rememberNumber)
        }
        if (calculate.operand2 !== '' && calculate.operand1 !== '' && calculate.result == '') {
            calculate.rememberNumber = eval(`${calculate.rememberNumber}+${calculate.operand2}`)
            console.log('m+/operand2' + calculate.rememberNumber)
        }
        if (calculate.result !== '' && calculate.operand1 !== '' && calculate.operand2 !== '') {
            calculate.rememberNumber = eval(`${calculate.rememberNumber}+${calculate.result}`)
        }
    } else if (value === 'm-') {
        if (calculate.operand1 !== '' && calculate.operand2 == '') {
            calculate.rememberNumber = eval(`${calculate.rememberNumber}-${calculate.operand1}`)
            console.log('m-/operand1' + calculate.rememberNumber)
        }
        if (calculate.operand2 !== '' && calculate.operand1 !== '' && calculate.result == '') {
            calculate.rememberNumber = eval(`${calculate.rememberNumber}-${calculate.operand2}`)
            console.log('m-/operand2' + calculate.rememberNumber)
        }
        if (calculate.result !== '' && calculate.operand1 !== '' && calculate.operand2 !== '') {
            calculate.rememberNumber = eval(`${calculate.rememberNumber}-${calculate.result}`)
        }
    } else if (value === 'mrc') {
        calculate.mrc = calculate.rememberNumber;
    }
}

function show() {
    let disp = document.querySelector('.display > input');
    if (calculate.operand1 !== '') {
        disp.value = calculate.operand1;
        console.log('operand1' + calculate.operand1)
    }
    if (calculate.sign !== '') {
        disp.value = calculate.sign;
        console.log('sign' + calculate.sign)
    }
    if (calculate.operand2 !== '') {
        disp.value = calculate.operand2;
        console.log('operand2' + calculate.operand2)
    }
    if (calculate.result !== '') {
        disp.value = calculate.result;
    }
    if (calculate.mrc !== '') {
        disp.value = calculate.mrc;
    }
}
function reset() {
    calculate = {
        operand1: '',
        operand2: '',
        sign: '',
        result: '',
        rememberNumber: '0',
        mrc: ''
    };
    document.querySelector('.display > input').value = ''
}