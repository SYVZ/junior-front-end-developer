document.addEventListener('DOMContentLoaded', () => {
    //DragAndDrope//
    // Пицца клиента 
    const clientPizza = {
        size: null,
        sauce: [],
        nameSauce: [],
        topping: [],
        nameTopping: [],
        price: 0,
    };
    //================================================//
    //Стоимость пиццы 
    const pricePizza = {
        largePizza: 200,
        middlePizza: 150,
        smallPizza: 100,
        ketchup: 50,
        bbq: 50,
        ricotta: 50,
        ordinaryCheese: 25,
        fetaCheese: 25,
        mozzarellaCheese: 25,
        meat: 50,
        tomatoes: 25,
        mushroom: 25

    };

    let inputRadio = document.querySelectorAll(".radioIn")
    inputRadio.forEach((e) => {
        e.addEventListener("click", () => {
            clientPizza.size = e.value;
            console.dir(clientPizza.size)
            myPizza()
        });


    })


    function costPizza() {
        clientPizza.price = 0;
        if (clientPizza.size) {
            if (clientPizza.size === "small") {
                clientPizza.price += pricePizza.smallPizza;
            };
            if (clientPizza.size === "mid") {
                clientPizza.price += pricePizza.middlePizza;
            };
            if (clientPizza.size === "big") {
                clientPizza.price += pricePizza.largePizza;
            };
        };

        clientPizza.sauce.forEach(element => {
            if (element === "sauceClassic") {
                clientPizza.price += pricePizza.ketchup;
            };
            if (element === "sauceBBQ") {
                clientPizza.price += pricePizza.bbq;
            };
            if (element === "sauceRikotta") {
                clientPizza.price += pricePizza.ricotta;
            }
        });

        clientPizza.topping.forEach(element => {
            if (element === "moc1") {
                clientPizza.price += pricePizza.ordinaryCheese;
            };
            if (element === "moc2") {
                clientPizza.price += pricePizza.fetaCheese;
            };
            if (element === "moc3") {
                clientPizza.price += pricePizza.mozzarellaCheese;
            };
            if (element === "telya") {
                clientPizza.price += pricePizza.meat;
            };
            if (element === "vetch1") {
                clientPizza.price += pricePizza.tomatoes;
            };
            if (element === "vetch1") {
                clientPizza.price += pricePizza.mushroom;
            };
        });

        console.dir(clientPizza.price)
    };

    function price() {
        if (clientPizza.price !== 0) {
            document.querySelector('.price > p').textContent = `Цiна: ${clientPizza.price}грн`;
        } else document.querySelector('.price > p').textContent = `Цiна:`;
    };
    function sauces() {
        document.querySelector('.sauces > p').textContent = `Соуси: ${clientPizza.nameSauce.join(",")}`;
    };
    function topings() {
        document.querySelector('.topings > p').textContent = `Топiнги: ${clientPizza.nameTopping.join(",")}`;
    };
    function myPizza() {
        costPizza();
        price();
        sauces();
        topings();
    };



    //==================================================//
    //DragandDrop
    const dragAndDrop = () => {
        const defaultPizza = document.querySelector(".table-wrapper .table");

        // dragstart - вызывается в самом начале переноса перетаскиваемого элемента.
        function dragStart(e) {
            const ingridient = e.target.dataset.key;
            if (ingridient === "sauce") {
                if (!clientPizza.nameSauce.includes(e.target.alt)) {
                    clientPizza.nameSauce.push(e.target.alt);
                    clientPizza.sauce.push(e.target.id);


                }

            };
            if (ingridient === "topping") {
                if (!clientPizza.nameTopping.includes(e.target.alt)) {
                    clientPizza.nameTopping.push(e.target.alt);
                    clientPizza.topping.push(e.target.id)

                }
            };
            e.dataTransfer.effectAllowed = "move";
            e.dataTransfer.setData("img", this.attributes.src.textContent)

        };
        // dragend - вызывается в конце события перетаскивания - как успешного, так и отмененного.
        function dragEnd(e) {
            defaultPizza.classList.remove("hovered");
            if (e.preventDefault) e.preventDefault();
            if (e.stopPropagation) e.stopPropagation();
        };
        // dragenter - происходит в момент когда перетаскиваемый объект попадает в область целевого элемента.
        function dragEnter(e) {
            defaultPizza.classList.add("hovered");
            if (e.preventDefault) e.preventDefault();
            if (e.stopPropagation) e.stopPropagation();
        };
        // dragleave - происходит когда перетаскиваемый элемент покидает область целевого элемента.
        function dragLeave(e) {
            defaultPizza.classList.remove("hovered");
            if (e.preventDefault) e.preventDefault();
            if (e.stopPropagation) e.stopPropagation();

        };
        // dragover - происходит когда перетаскиваемый элемент находиться над целевым элементом.
        function dragOver(e) {
            if (e.preventDefault) e.preventDefault();
            if (e.stopPropagation) e.stopPropagation();

        };
        // drop - вызывается, когда событие перетаскивания завершается отпусканием элемента над целевым элементом.
        function drop(e) {
            if (e.preventDefault) e.preventDefault();
            if (e.stopPropagation) e.stopPropagation();

            const imgIngridient = document.createElement("img");
            imgIngridient.setAttribute("src", e.dataTransfer.getData("img"));
            defaultPizza.append(imgIngridient);

            myPizza()

        };

        defaultPizza.addEventListener("dragover", dragOver);
        defaultPizza.addEventListener("drop", drop);
        defaultPizza.addEventListener("dragenter", dragEnter);
        defaultPizza.addEventListener("dragleave", dragLeave);


        const ingridients = document.querySelectorAll(".ingridients .draggable");
        ingridients.forEach(ingridient => {
            ingridient.addEventListener("dragstart", dragStart);
            ingridient.addEventListener("dragend", dragEnd);

        });

    }
    dragAndDrop();
    //=============================================//
    // Убегающий баннер//
    const banner = document.querySelector("#banner");

    let maxRight = document.documentElement.clientWidth - banner.offsetWidth,
        maxTop = document.documentElement.clientHeight - banner.offsetHeight;

    banner.addEventListener("mousemove", () => {
        let right = Math.random() * maxRight;
        banner.style.right = right + "px";
        let top = Math.random() * maxTop;
        banner.style.bottom = top + "px";
        console.log(right + " - " + top);

    });
    //==============================================//
    //Кнопка сброса 
    const forms = document.forms[1];
    let resetButton = document.querySelector("[type=reset]");

    resetButton.addEventListener("mousedown", (e) => {
        e.target.classList.add("buttonclick")

    });

    resetButton.addEventListener("mouseup", (e) => {
        e.target.classList.remove("buttonclick")

    });

    forms.addEventListener("reset", () => {

        clientPizza.size = null;
        clientPizza.sauce = [];
        clientPizza.nameSauce = [];
        clientPizza.topping = [];
        clientPizza.nameTopping = [];
        clientPizza.price = 0;

        const ingridients = document.querySelectorAll('.table img');
        ingridients.forEach(ingridient => {
            if (!ingridient.alt[0]) {
                ingridient.remove();
            }
        });

        for (let i = 0; i < forms.length; i++) {
            if (forms[i].type === "text" || forms[i].type === "tel" || forms[i].type === "email") {
                forms[i].classList.add("default");
                forms[i].classList.remove("error");
                forms[i].classList.remove("success");
            }
        };
        myPizza()

    });
    //================================================//
    // Проверка валидации
    const submitButton = document.querySelector("[type=submit]");
    let flagCheck = false;

    submitButton.addEventListener("mousedown", (e) => {
        e.target.classList.add("buttonclick")

    });

    submitButton.addEventListener("mouseup", (e) => {
        e.target.classList.remove("buttonclick")

    });

    forms.addEventListener("submit", formValidate)

    for (let i = 0; i < forms.length; i++) {
        forms[i].addEventListener("change", validateInput)
    };

    function validateInput() {
        if (this.type === "text") {
            if (/^[А-яіїґє-]+$/.test(this.value) === true) {
                this.classList.remove("error");
                this.classList.remove("default");
                this.classList.add("success");
                flagCheck = true;
            } if (/^[А-яіїґє-]+$/.test(this.value) === false) {
                this.classList.remove("success");
                this.classList.remove("default");
                this.classList.add("error");
                flagCheck = false;
            } if (this.value === "") {
                this.classList.remove("success");
                this.classList.remove("default");
                this.classList.remove("error");
                flagCheck = false;
            }
        };
        if (this.type === "tel") {
            if (/^\+380[0-9]{9}$/.test(this.value) === true) {
                this.classList.remove("error");
                this.classList.add("success");
                this.classList.remove("default");
                flagCheck = true;
            } if (/^\+380[0-9]{9}$/.test(this.value) === false) {
                this.classList.remove("success");
                this.classList.add("error");
                this.classList.remove("default");
                flagCheck = false;
            } if (this.value === "") {
                this.classList.remove("success");
                this.classList.remove("error");
                this.classList.remove("default");
                flagCheck = false;
            }
        };
        if (this.type === "email") {
            if (/\b[a-z0-9._]+@[a-z0-9.-]+\.[a-z]{2,4}\b/.test(this.value) === true) {
                this.classList.remove("error");
                this.classList.add("success");
                this.classList.remove("default");
                flagCheck = true;
            } if (/\b[a-z0-9._]+@[a-z0-9.-]+\.[a-z]{2,4}\b/.test(this.value) === false) {
                this.classList.remove("success");
                this.classList.add("error");
                this.classList.remove("default");
                flagCheck = false;
            } if (this.value === "") {
                this.classList.remove("success");
                this.classList.remove("error");
                this.classList.remove("default");
                flagCheck = false;
            }
        };
    }

    function formValidate(e) {
        e.preventDefault();
        if (flagCheck === true && clientPizza.size !== null) {
            forms.submit();
            window.location.href = "./thank-you.html";
        } else if (flagCheck === true && clientPizza.size === null) {
            console.log("errorSize")
            alert("Выберите размер пиццы!")
        }
        else if (flagCheck === false && clientPizza.size !== null) {
            console.log("errorvalid")
            alert("Форма отправки заполена не верно!")
        }
    };




















})