let slides = document.querySelectorAll('#slides .slide');
let startButton = document.querySelector('.start');
let stopButton = document.querySelector('.stop');
let leftButton = document.querySelector('.left');
let rightButton = document.querySelector('.right');
let continueButton = document.querySelector('.continue');
let currentSlide = 0;
let slideInterval

function nextSlide() {
    slides[currentSlide].className = 'slide';
    currentSlide = (currentSlide + 1) % slides.length;
    slides[currentSlide].className = 'slide showing'
};

startButton.addEventListener('click', () => {
    slideInterval = setInterval(nextSlide, 2000);
});

stopButton.addEventListener('click', () => {
    clearInterval(slideInterval)
});

continueButton.addEventListener('click', () => {
    slideInterval = setInterval(nextSlide, 2000);
});

leftButton.addEventListener('click', () => {
    if (currentSlide !== 0) {
        slides[currentSlide].className = 'slide';
        currentSlide = (currentSlide - 1) % slides.length;
        slides[currentSlide].className = 'slide showing'
    }
    if (currentSlide == 0) {
        slides[currentSlide].className = 'slide';
        currentSlide = slides.length - 1;
        slides[currentSlide].className = 'slide showing'
    }

});

rightButton.addEventListener('click', () => {
    slides[currentSlide].className = 'slide';
    currentSlide = (currentSlide + 1) % slides.length;
    slides[currentSlide].className = 'slide showing'

});