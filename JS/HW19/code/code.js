document.addEventListener('DOMContentLoaded', () => {
    const grid = document.querySelector('.grid')
    const qualityBomb = document.querySelector('#quality-bomb')
    const result = document.querySelector('#result')
    let width = 10
    let bombAmount = 20
    let flags = 0
    let squares = []
    let isGameOver = false
    let isRestart = false;

    //создать доску
    function createBoard() {
        qualityBomb.innerHTML = bombAmount
        //Получаем перемешанны игровой массив со случайными бомбами  
        const bombsArray = Array(bombAmount).fill('bomb')
        const emptyArray = Array(width * width - bombAmount).fill('valid')
        console.log(bombsArray)
        console.log(emptyArray)
        const gameArray = emptyArray.concat(bombsArray)
        console.log(gameArray)
        const shuffledArray = gameArray.sort(() => Math.random() - 0.5)
        console.log(shuffledArray)


        for (let i = 0; i < width * width; i++) {
            const square = document.createElement('div')
            square.setAttribute('id', i)
            square.classList.add(shuffledArray[i])
            grid.appendChild(square)
            squares.push(square)

            //обычный клик 
            square.addEventListener('click', function (e) {
                click(square)
                if (!isRestart) restartGame();
            })

            //сtrl и левый клик 
            square.oncontextmenu = function (e) {
                e.preventDefault()
                addFlag(square)
            }
        }
        //Добовлям числа 
        for (let i = 0; i < squares.length; i++) {
            let total = 0
            const isLeftEdge = (i % width === 0)
            const isRightEdge = (i % width === width - 1)

            if (squares[i].classList.contains('valid')) {
                if (i > 0 && !isLeftEdge && squares[i - 1].classList.contains('bomb')) total++
                if (i > 9 && !isRightEdge && squares[i + 1 - width].classList.contains('bomb')) total++
                if (i > 10 && squares[i - width].classList.contains('bomb')) total++
                if (i > 11 && !isLeftEdge && squares[i - 1 - width].classList.contains('bomb')) total++
                if (i < 98 && !isRightEdge && squares[i + 1].classList.contains('bomb')) total++
                if (i < 90 && !isLeftEdge && squares[i - 1 + width].classList.contains('bomb')) total++
                if (i < 88 && !isRightEdge && squares[i + 1 + width].classList.contains('bomb')) total++
                if (i < 89 && squares[i + width].classList.contains('bomb')) total++
                squares[i].setAttribute('data', total)
                console.log(squares[i])
            }
        }




    }

    createBoard()
    //добавить кнопку рестарт 
    function restartGame() {
        let restartGame = document.createElement('input')
        restartGame.setAttribute('type', 'button')
        restartGame.setAttribute('value', 'Pестарт')
        restartGame.setAttribute('onClick', 'window.location.reload()')
        qualityBomb.after(restartGame)
        isRestart = true;
    }

    //добавить флаг правым кликом 
    function addFlag(square) {
        if (isGameOver) return
        if (!square.classList.contains('checked') && (flags < bombAmount)) {
            if (!square.classList.contains('flag')) {
                square.classList.add('flag')
                square.innerHTML = ' 🚩'
                flags++
                qualityBomb.innerHTML = bombAmount - flags
                checkForWin()
            } else {
                square.classList.remove('flag')
                square.innerHTML = ''
                flags--
                qualityBomb.innerHTML = bombAmount - flags
            }

        }
    }

    //клик на square действие 
    function click(square) {
        let currentId = square.id
        if (isGameOver) return
        if (square.classList.contains('checked') || square.classList.contains('flag')) return
        if (square.classList.contains('bomb')) {
            gameOver(square)
        } else {
            let total = square.getAttribute('data')
            if (total != 0) {
                square.classList.add('checked')
                if (total == 1) square.classList.add('one')
                if (total == 2) square.classList.add('two')
                if (total == 3) square.classList.add('three')
                if (total == 4) square.classList.add('four')
                if (total == 5) square.classList.add('five')
                square.innerHTML = total
                return
            }
            checkSquare(square, currentId)
        }
        square.classList.add('checked')
    }

    //проверить соседний квадрат,один квадрат выбран 
    function checkSquare(square, currentId) {
        const isLeftEdge = (currentId % width === 0)
        const isRightEdge = (currentId % width === -1)

        setTimeout(() => {
            if (currentId > 0 && !isLeftEdge) {
                const newId = squares[parseInt(currentId) - 1].id
                const newSquare = document.getElementById(newId)
                click(newSquare)

            }
            if (currentId > 9 && !isRightEdge) {
                const newId = squares[parseInt(currentId) + 1 - width].id
                const newSquare = document.getElementById(newId)
                click(newSquare)

            }
            if (currentId > 10) {
                const newId = squares[parseInt(currentId - width)].id
                const newSquare = document.getElementById(newId)
                click(newSquare)
            }
            if (currentId > 11 && !isLeftEdge) {
                const newId = squares[parseInt(currentId) - 1 - width].id
                const newSquare = document.getElementById(newId)
                click(newSquare)
            }
            if (currentId < 98 && !isRightEdge) {
                const newId = squares[parseInt(currentId) + 1].id
                const newSquare = document.getElementById(newId)
                click(newSquare)

            }
            if (currentId < 90 && !isLeftEdge) {
                const newId = squares[parseInt(currentId) - 1 + width].id
                const newSquare = document.getElementById(newId)
                click(newSquare)

            }
            if (currentId < 88 && !isLeftEdge) {
                const newId = squares[parseInt(currentId) + 1 + width].id
                const newSquare = document.getElementById(newId)
                click(newSquare)

            }
            if (currentId < 89) {
                const newId = squares[parseInt(currentId) + width].id
                const newSquare = document.getElementById(newId)
                click(newSquare)

            }
        }, 10)
    }

    //Игра окончена 
    function gameOver(square) {
        console.log('BOOM! Game Over')
        result.innerHTML = 'БУМ!!!Ты проиграл!'
        isGameOver = true

        // Показать все бомбы
        squares.forEach(square => {
            if (square.classList.contains('bomb')) {
                square.innerHTML = '💣'
            }
        })
    }

    //Победа
    function checkForWin() {
        let matches = 0
        for (let i = 0; i < squares.length; i++) {
            if (squares[i].classList.contains('flag') && squares[i].classList.contains('bomb')) {
                matches++
            }
            if (matches === bombAmount) {
                console.log('YOU WIN')
                result.innerHTML = 'Победа!'
                isGameOver = true
            }
        }
    }


})
